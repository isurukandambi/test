﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestRepoTests1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void PossitiveSchenarioForChecking_combineArrayStringWithSpace()
        {
            string expectedResult = "Today is the wonderful day of my life";
            string[] actualStringArray = new string[] { "Today", "is", "the", "wonderful", "day", "of", "my", "life" };
            ApplicationCodeClass appObject = new ApplicationCodeClass();

            string actualResult = appObject.combineArrayStringWithSpace(actualStringArray);
            Assert.AreEqual<string>(expectedResult, actualResult);
        }
    }
}
